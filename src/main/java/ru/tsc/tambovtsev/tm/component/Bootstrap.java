package ru.tsc.tambovtsev.tm.component;

import ru.tsc.tambovtsev.tm.api.controller.ICommandController;
import ru.tsc.tambovtsev.tm.api.controller.IProjectController;
import ru.tsc.tambovtsev.tm.api.controller.ITaskController;
import ru.tsc.tambovtsev.tm.api.repository.ICommandRepository;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.ICommandService;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.constant.ArgumentConst;
import ru.tsc.tambovtsev.tm.constant.TerminalConst;
import ru.tsc.tambovtsev.tm.controller.CommandController;
import ru.tsc.tambovtsev.tm.controller.ProjectController;
import ru.tsc.tambovtsev.tm.controller.TaskController;
import ru.tsc.tambovtsev.tm.repository.CommandRepository;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.TaskRepository;
import ru.tsc.tambovtsev.tm.service.CommandService;
import ru.tsc.tambovtsev.tm.service.ProjectService;
import ru.tsc.tambovtsev.tm.service.TaskService;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (processArgument(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showTaskList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createTask();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearTasks();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void close() {
        System.exit(0);
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}