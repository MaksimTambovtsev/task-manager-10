package ru.tsc.tambovtsev.tm.controller;

import ru.tsc.tambovtsev.tm.api.controller.IProjectController;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
