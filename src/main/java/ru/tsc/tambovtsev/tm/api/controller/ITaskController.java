package ru.tsc.tambovtsev.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTasks();

    void createTask();

}
