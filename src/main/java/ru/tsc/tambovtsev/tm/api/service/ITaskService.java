package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    List<Task> findAll();

    void remove(Task task);

    Task create(String name, String description);

    Task add(Task task);

    void clear();

}
