package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    List<Project> findAll();

    void remove(Project project);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

}
