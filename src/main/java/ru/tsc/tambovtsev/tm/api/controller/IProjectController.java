package ru.tsc.tambovtsev.tm.api.controller;

public interface IProjectController {

    void showTaskList();

    void clearTasks();

    void createTask();

}
